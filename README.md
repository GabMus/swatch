# <a href="https://swatch.gabmus.org"><img height="32" src="data/icons/org.gabmus.swatch.svg" /> Swatch</a>

Color palette manager.

<!-- ![screenshot](https://gitlab.gnome.org/GabMus/swatch/-/raw/website/static/screenshots/mainwindow.png) -->

## Dependencies

- python 3.9
- Gtk4
- [libadwaita](https://gitlab.gnome.org/GNOME/libadwaita)
<!-- - [python-pillow](https://github.com/python-pillow/Pillow) -->

<a href="https://flathub.org/apps/details/org.gabmus.swatch"><img height="60" src="https://raw.githubusercontent.com/flatpak-design-team/flathub-mockups/master/assets/download-button/download.svg?sanitize=true" alt="Download on Flathub" /></a>

## Build and run from source

**Note**: If you're looking to install the app for regular use, [download it from Flathub](https://flathub.org/apps/details/org.gabmus.swatch) or from your distribution repositories. These instructions are only for developers and package maintainers.

```bash
git clone https://gitlab.gnome.org/gabmus/swatch
cd swatch
mkdir build
cd build
meson .. -Dprefix="$PWD/build/mprefix"
ninja
ninja install
ninja run
```

## Hacking

You might want to check your code with [flake8](https://github.com/pycqa/flake8) before opening a merge request.

```bash
flake8 swatch
```
