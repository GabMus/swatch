from gi.repository import Gtk, Gdk, GObject
from swatch.action_helper import new_action, new_action_group
from swatch.color import Color
from swatch.color_button import ColorButton


@Gtk.Template(resource_path='/org/gabmus/swatch/ui/color_list_row.ui')
class ColorListRow(Gtk.ListBoxRow):
    __gtype_name__ = 'ColorListRow'
    color = None
    name_stack: Gtk.Stack = Gtk.Template.Child()
    name_label: Gtk.Label = Gtk.Template.Child()
    name_entry: Gtk.Entry = Gtk.Template.Child()
    name_entry_box: Gtk.Entry = Gtk.Template.Child()
    hex_label: Gtk.Label = Gtk.Template.Child()
    color_btn: ColorButton = Gtk.Template.Child()
    __gsignals__ = {
        'delete-request': (
            GObject.SignalFlags.RUN_LAST,
            None,
            tuple()
        )
    }

    def __init__(self):
        super().__init__()
        new_action_group(self, 'colorrow', [
            new_action('rename', self.action_rename),
            new_action('delete', self.action_delete),
            new_action('copyrgb', self.action_copyrgb)
        ])

    def set_color(self, color: Color):
        self.color = color
        self.name_label.set_text(color.name)
        self.hex_label.set_text(self.color.hex())
        self.color_btn.set_color(self.color)
        self.color.connect('notify::name', self.on_color_name_changed)
        self.color.connect('notify::color', self.on_color_changed)

    def on_color_changed(self, *_):
        self.hex_label.set_text(self.color.hex())
        root = self.get_root()
        if root:
            root.save_palette()

    def on_color_name_changed(self, *_):
        self.name_label.set_text(self.color.name)
        root = self.get_root()
        if root:
            root.save_palette()

    @Gtk.Template.Callback()
    def on_copy_btn_clicked(self, *_):
        if not self.color:
            return
        Gdk.Display.get_default().get_clipboard().set(
            self.color.hex()
        )

    def action_rename(self, *_):
        self.name_entry.set_text(self.color.name)
        self.name_stack.set_visible_child(self.name_entry_box)
        self.name_entry.grab_focus()

    def action_delete(self, *_):
        self.emit('delete-request')

    def action_copyrgb(self, *_):
        if not self.color:
            return
        rgb = 'rgb({0}, {1}, {2})'.format(*self.color.color)
        Gdk.Display.get_default().get_clipboard().set(rgb)

    @Gtk.Template.Callback()
    def on_name_entry_confirm_btn_clicked(self, *_):
        n_name = self.name_entry.get_text()
        if not n_name:
            return
        self.color.name = n_name
        self.name_stack.set_visible_child(self.name_label)

    @Gtk.Template.Callback()
    def on_name_entry_activate(self, *_):
        self.on_name_entry_confirm_btn_clicked()
