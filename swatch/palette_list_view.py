from gettext import gettext as _
from gi.repository import Gtk, Adw
from swatch.confManager import ConfManager
from swatch.palette import Palette
from swatch.palette_list_view_row import PaletteListViewRow
from swatch.palette_store import PaletteStore


@Gtk.Template(resource_path='/org/gabmus/swatch/ui/palette_list_view.ui')
class PaletteListView(Adw.Bin):
    __gtype_name__ = 'PaletteListView'
    listbox: Gtk.ListBox = Gtk.Template.Child()
    stack: Gtk.Stack = Gtk.Template.Child()
    listbox_clamp: Adw.Clamp = Gtk.Template.Child()
    empty_state: Adw.StatusPage = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.confman = ConfManager()
        self.palette_store = PaletteStore()
        self.listbox.bind_model(
            self.palette_store, self.__create_palette_row, None
        )
        self.palette_store.connect('items-changed', self.on_items_changed)
        self.on_items_changed()

    def on_items_changed(self, *_):
        self.stack.set_visible_child(
            self.empty_state if len(self.palette_store) <= 0
            else self.listbox_clamp
        )

    def __create_palette_row(self, palette: Palette, *_) -> PaletteListViewRow:
        row = PaletteListViewRow()
        row.set_palette(palette)
        row.connect('delete-request', self.on_palette_delete_request)
        return row

    def on_palette_delete_request(self, __, palette: Palette):
        if palette is None:
            return
        dialog = Gtk.MessageDialog(
            transient_for=self.get_root(),
            modal=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            text=_('Do you want to delete "{0}"?').format(
                palette.name
            )
        )

        def on_response(_dialog, res):
            _dialog.close()
            if res == Gtk.ResponseType.YES:
                Gtk.Application.get_default().close_palette_window(palette)
                self.palette_store.remove_palette(palette)

        dialog.connect('response', on_response)
        dialog.present()

    def add_palette(self):
        n = 1
        basename = _('Palette {0}')
        name = basename.format(n)
        while True:
            if name in self.confman.conf['palettes'].keys():
                n += 1
                name = basename.format(n)
            else:
                break
        self.palette_store.add_palette(Palette(name))

    @Gtk.Template.Callback()
    def on_listbox_row_activated(self, _, row: PaletteListViewRow):
        if not row.palette:
            return
        Gtk.Application.get_default().open_palette(row.palette)
        self.get_root().close()
