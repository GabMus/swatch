from gettext import gettext as _
from gi.repository import Gtk, Adw
from swatch.action_helper import new_action, new_action_group
from swatch.palette_view import PaletteView
from swatch.color import Color
from swatch.palette import Palette
from swatch.view_switch_button import ViewSwitchButton


@Gtk.Template(resource_path='/org/gabmus/swatch/ui/palette_window.ui')
class PaletteWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'PaletteWindow'
    palette_view: PaletteView = Gtk.Template.Child()
    title_stack: Gtk.Stack = Gtk.Template.Child()
    title_entry_box = Gtk.Template.Child()
    title_label_box = Gtk.Template.Child()
    title_label: Gtk.Label = Gtk.Template.Child()
    title_entry: Gtk.Entry = Gtk.Template.Child()

    def __init__(self, palette: Palette):
        self.palette = palette
        super().__init__(title=self.palette.name)
        self.set_palette_name(self.palette.name)
        self.palette.connect(
            'notify::name',
            lambda *_: self.set_palette_name(self.palette.name)
        )
        self.palette_view.set_palette(palette)
        new_action_group(self, 'palettewin', [
            new_action('export_gimp', self.export_gimp_palette)
        ])

    def export_gimp_palette(self, *__):
        self.export_file_chooser = Gtk.FileChooserNative(
            title=_('Export as GIMP Palette'),
            transient_for=self, modal=True,
            action=Gtk.FileChooserAction.SAVE,
            accept_label=_('Save'),
            cancel_label=_('Cancel'),
            create_folders=True
        )
        self.export_file_chooser.set_current_name('{0}.gpl'.format(
            self.palette.name.replace('/', '_').replace(':', '_')
        ))

        def on_response(_dialog, res):
            if res == Gtk.ResponseType.ACCEPT:
                with open(_dialog.get_file().get_path(), 'w') as fd:
                    fd.write(self.palette.to_gimp_palette())

        self.export_file_chooser.connect('response', on_response)
        self.export_file_chooser.show()

    def present(self):
        super().present()
        self.set_default_size(
            self.palette.window_size[0],
            self.palette.window_size[1]
        )

    def set_palette_name(self, name: str):
        self.set_title(name)
        self.title_label.set_text(name)
        self.title_entry.set_text(name)

    @Gtk.Template.Callback()
    def on_add_btn_clicked(self, *_):
        color = Color(255, 255, 255)
        self.palette.color_store.add_color(color)
        self.save_palette()

    @Gtk.Template.Callback()
    def on_view_switch_btn_clicked(self, btn: ViewSwitchButton):
        btn.set_view_list(not btn.get_view_list())
        self.palette_view.set_view_list(btn.get_view_list())

    @Gtk.Template.Callback()
    def on_edit_title_btn_clicked(self, *_):
        self.title_stack.set_visible_child(self.title_entry_box)

    @Gtk.Template.Callback()
    def on_title_edit_confirm_btn_clicked(self, *_):
        n_name = self.title_entry.get_text().strip()
        if not n_name:
            return
        self.palette.name = n_name
        self.title_stack.set_visible_child(self.title_label_box)
        self.save_palette()

    @Gtk.Template.Callback()
    def on_title_entry_activate(self, *_):
        self.on_title_edit_confirm_btn_clicked()

    def save_palette(self):
        self.palette.save_to_config(
            window_size=(self.get_width(), self.get_height())
        )

    @Gtk.Template.Callback()
    def on_close_request(self, *_):
        self.save_palette()
        self.destroy()
