from gi.repository import Gtk
from swatch.color_bin import ColorBin


class ColorButton(ColorBin):
    __gtype_name__ = 'ColorButton'

    def __init__(self):
        super().__init__()
        self.btn = Gtk.Button(hexpand=True, vexpand=True)
        self.btn.get_style_context().add_class('transparent-btn')
        self.set_child(self.btn)
        self.btn.connect('clicked', self.on_clicked)

    def on_clicked(self, *_):
        chooser = Gtk.ColorChooserDialog(
            show_editor=True, rgba=self.color.get_rgba(), modal=True,
            transient_for=self.get_root()
        )

        def on_response(_chooser, res):
            _chooser.close()
            if res == Gtk.ResponseType.OK:
                rgba = _chooser.get_rgba()
                self.color.set_rgba(rgba)

        chooser.connect('response', on_response)
        chooser.present()
