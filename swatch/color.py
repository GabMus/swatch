from typing import Tuple
from math import floor
from gi.repository import GObject, Gdk


class Color(GObject.Object):
    def __init__(self, red: int, green: int, blue: int):
        for c in (red, green, blue):
            assert (c >= 0 and c <= 255)
        self.__gcolor = Gdk.RGBA()
        self.__gcolor.parse(f'rgb({red},{green},{blue})')
        self.__name = ''
        super().__init__()

    def __eq__(self, other):
        return self.__name == other.__name and self.hex() == other.hex()

    def __ne__(self, other):
        return not self == other

    def get_floats(self) -> Tuple[float, float, float]:
        return (self.__gcolor.red, self.__gcolor.green, self.__gcolor.blue)

    def __to_scale(self, value: float, scale: float = 255) -> int:
        # ripped from https://gitlab.gnome.org/GNOME/gtk
        # /-/blob/main/gtk/gtkcoloreditor.c#L97
        value = floor(value * scale + 0.5)
        value = max(value, 0)
        value = min(value, scale)
        return int(value)

    def hex(self) -> str:
        return '#{:02x}{:02x}{:02x}'.format(
            *[
                self.__to_scale(channel) for channel in
                (self.__gcolor.red, self.__gcolor.green, self.__gcolor.blue)
            ]
        )

    @GObject.Property()
    def color(self) -> Tuple[int, int, int]:
        return tuple(self.__to_scale(channel) for channel in self.get_floats())

    @color.setter
    def color(self, n_color: Tuple[float, float, float]):
        self.__gcolor.red = n_color[0]
        self.__gcolor.green = n_color[1]
        self.__gcolor.blue = n_color[2]

    def set_rgba(self, rgba: Gdk.RGBA):
        self.color = (rgba.red, rgba.green, rgba.blue)

    @GObject.Property(type=str)
    def name(self) -> str:
        return self.__name or self.hex()

    @name.setter
    def name(self, n_name: str):
        self.__name = n_name

    def get_rgba(self) -> Gdk.RGBA:
        return self.__gcolor
