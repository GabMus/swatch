from gi.repository import Gtk, Gio
from swatch.confManager import ConfManager
from swatch.palette import Palette


class PaletteStore(Gtk.SortListModel):
    def __init__(self):
        self.sorter = Gtk.CustomSorter()
        self.sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=Palette)
        super().__init__(model=self.list_store, sorter=self.sorter)
        self.confman = ConfManager()
        self.populate()

    def populate(self):
        for name in self.confman.conf['palettes'].keys():
            palette = Palette(name)
            palette.populate()
            self.add_palette(palette, startup=True)

    def __sort_func(self, p1: Palette, p2: Palette, *_) -> int:
        return -1 if p1.name < p2.name else 1

    def invalidate_sort(self):
        self.sorter.set_sort_func(self.__sort_func)

    def add_palette(self, n_palette: Palette, startup: bool = False):
        n_palette.connect(
            'notify::name', lambda *_: self.invalidate_sort()
        )
        self.list_store.append(n_palette)
        if not startup:
            n_palette.save_to_config()

    def remove_palette(self, target: Palette):
        for i, palette in enumerate(self.list_store):
            if palette == target:
                self.list_store.remove(i)
                self.confman.conf['palettes'].pop(target.name)
                self.confman.save_conf()
