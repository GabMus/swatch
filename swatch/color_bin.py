from gi.repository import Gtk, Adw
from swatch.color import Color


STYLE_CLASSES_CACHE = list()


class ColorBin(Adw.Bin):
    __gtype_name__ = 'ColorBin'
    color = Color(255, 255, 255)
    style_class = None

    def __init__(self):
        super().__init__(
            overflow=Gtk.Overflow.HIDDEN,
            hexpand=False, vexpand=False
        )
        self.style_context = self.get_style_context()
        self.style_context.add_class('card')

    def set_color(self, color: Color):
        self.color = color
        self.set_style_class()
        self.color.connect(
            'notify::color', lambda *_: self.set_style_class()
        )

    def set_style_class(self):
        if self.style_class is not None:
            self.style_context.remove_class(self.style_class)
        class_name = f'colorbtn-{self.color.hex().lstrip("#")}'
        if class_name not in STYLE_CLASSES_CACHE:
            css_provider = Gtk.CssProvider()
            css = f'''.{class_name} {{
                background-color: {self.color.hex()};
            }}'''
            css_provider.load_from_data(css, len(css))
            Gtk.StyleContext().add_provider_for_display(
                self.get_display(), css_provider,
                Gtk.STYLE_PROVIDER_PRIORITY_USER
            )
            STYLE_CLASSES_CACHE.append(class_name)
        self.style_context.add_class(class_name)
        self.style_class = class_name
