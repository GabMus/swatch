import sys
# import argparse
from gettext import gettext as _
from typing import List
from gi.repository import Gtk, Gio
from swatch.color import Color
from swatch.confManager import ConfManager
from swatch.app_window import AppWindow
from swatch.palette import Palette
from swatch.palette_window import PaletteWindow
from swatch.preferences_window import PreferencesWindow
from swatch.base_app import BaseApp, AppAction


class GApplication(BaseApp):
    def __init__(self):
        super().__init__(
            app_id='org.gabmus.swatch',
            app_name='Swatch',
            app_actions=[
                AppAction(
                    name='preferences',
                    func=self.show_preferences_window,
                    accel='<Primary>comma'
                ),
                AppAction(
                    name='shortcuts',
                    func=self.show_shortcuts_window,
                    accel='<Primary>question'
                ),
                AppAction(
                  name='about',
                  func=self.show_about_dialog
                ),
                AppAction(
                    name='quit',
                    func=lambda *args: self.quit(),
                    accel='<Primary>q'
                ),
                AppAction(
                    name='open_start_window',
                    func=self.open_start_window,
                    accel='<Primary>o'
                ),
                AppAction(
                    name='import_gimp',
                    func=self.import_gimp_palette
                )
            ],
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            css_resource='/org/gabmus/swatch/ui/gtk_style.css'
        )
        self.confman = ConfManager()

    def quit(self, *args):
        self.confman.save_conf()
        super().quit()

    def show_about_dialog(self, *args):
        dialog = Gtk.Builder.new_from_resource(
            '/org/gabmus/swatch/aboutdialog.ui'
        ).get_object('aboutdialog')
        dialog.set_modal(True)
        dialog.set_transient_for(self.get_active_window())
        dialog.present()

    def show_shortcuts_window(self, *args):
        shortcuts_win = Gtk.Builder.new_from_resource(
            '/org/gabmus/swatch/ui/shortcutsWindow.ui'
        ).get_object('shortcuts-win')
        shortcuts_win.props.section_name = 'shortcuts'
        shortcuts_win.set_transient_for(self.get_active_window())
        shortcuts_win.set_modal(True)
        shortcuts_win.present()

    def show_preferences_window(self, *args):
        preferences_win = PreferencesWindow()
        preferences_win.set_transient_for(self.get_active_window())
        preferences_win.set_modal(True)
        preferences_win.present()

    def do_activate(self):
        super().do_activate()
        win = AppWindow()
        self.add_window(win)
        win.present()
        if hasattr(self, 'args'):
            if self.args:
                pass

    def import_gimp_palette_from_lines(self, lines: List[str]):
        name = None
        clean_lines = list()
        for i, line in enumerate(lines):
            norm_line = line.strip().lower()
            if name is None and (
                    norm_line.startswith('#palette name:') or
                    norm_line.startswith('# palette name:')
            ):
                name = line.split(':')[-1].strip()
                continue
            if norm_line.startswith('#') or norm_line == 'gimp palette':
                continue
            clean_lines.append(line)
        if name is None:
            name = _('Palette')
        while name in self.confman.conf['palettes'].keys():
            name += _(' (copy)')
        palette = Palette(name)
        for line in clean_lines:
            sp = line.split()
            r, g, b = tuple(int(ch) for ch in sp[:3])
            color = Color(r, g, b)
            cname = ' '.join(sp[3:])
            if cname:
                color.name = cname
            palette.color_store.add_color(color)
        for win in self.get_windows():
            if isinstance(win, AppWindow):
                win.palette_list_view.palette_store.add_palette(palette)
                break

    def import_gimp_palette(self, *__):
        self.__import_fc = Gtk.FileChooserNative(
            title=_('Import GIMP Palette'),
            transient_for=self.get_active_window(),
            modal=True,
            action=Gtk.FileChooserAction.OPEN,
            accept_label=_('Open'),
            cancel_label=_('Cancel')
        )

        def on_response(_dialog, res):
            if res == Gtk.ResponseType.ACCEPT:
                with open(_dialog.get_file().get_path(), 'r') as fd:
                    self.import_gimp_palette_from_lines(fd.readlines())

        self.__import_fc.connect('response', on_response)
        self.__import_fc.show()

    def open_palette(self, palette: Palette):
        for win in self.get_windows():
            if isinstance(win, PaletteWindow) and win.palette == palette:
                win.present()
                return
        win = PaletteWindow(palette)
        self.add_window(win)
        win.present()

    def open_start_window(self, *_):
        if isinstance(self.get_active_window(), AppWindow):
            return
        win = AppWindow()
        self.add_window(win)
        win.present()

    def close_palette_window(self, target: Palette):
        for win in self.get_windows():
            if isinstance(win, PaletteWindow) and win.palette == target:
                win.close()

    def do_command_line(self, args):
        """
        GTK.Application command line handler
        called if Gio.ApplicationFlags.HANDLES_COMMAND_LINE is set.
        must call the self.do_activate() to get the application up and running.
        """
        # call the default commandline handler
        Gtk.Application.do_command_line(self, args)
        # make a command line parser
        #  #parser = argparse.ArgumentParser()
        #  #parser.add_argument(
        #  #    'argurl',
        #  #    metavar=_('url'),
        #  #    type=str,
        #  #    nargs='?',
        #  #    help=_('opml file local url or rss remote url to import')
        #  #)
        # parse the command line stored in args,
        # but skip the first element (the filename)
        #  #self.args = parser.parse_args(args.get_arguments()[1:])
        # call the main program do_activate() to start up the app
        self.do_activate()
        return 0


def main():

    application = GApplication()

    try:
        ret = application.run(sys.argv)
    except SystemExit as e:
        ret = e.code

    sys.exit(ret)


if __name__ == '__main__':
    main()
